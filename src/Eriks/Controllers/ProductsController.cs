﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using Eriks.Core;
using Eriks.Data.Access;
using Eriks.Data.Contracts.Entities;
using Eriks.Data.Contracts.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Eriks.Controllers {
    /// <summary>
    /// API for products management
    /// </summary>
    public sealed class ProductsController : ApiControllerBase<Product, string, ProductFilter> {
        public ProductsController(IProductsRepository repository, ILogger<ProductsController> logger) : base(repository, logger) {
        }
        protected override bool includeReferencesInListResults => true;
    }
}
