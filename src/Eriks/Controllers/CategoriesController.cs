﻿using CsvHelper.Configuration;
using Eriks.Core;
using Eriks.Data.Access;
using Eriks.Data.Contracts.Entities;
using Eriks.Data.Contracts.Models;
using Microsoft.Extensions.Logging;

namespace Eriks.Controllers {
    /// <summary>
    /// API for categories management
    /// </summary>
    public sealed class CategoriesController : ApiControllerBase<Category, string, CategoryFilter> {
        public CategoriesController(ICategoriesRepository repository, ILogger<CategoriesController> logger) : base(repository, logger) {
        }

    }
}
