﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using AutoMapper;
using Eriks.Core;
using Eriks.Data;
using Eriks.Data.Access;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Swagger;

namespace Eriks {
    public class Startup {
        private readonly string _apiVersion = "v1";
        private readonly string _appTitle = AppDomain.CurrentDomain.FriendlyName;

        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.AddCors(options => options.AddPolicy("AllowAll", p =>
                p.WithOrigins("https://eriks.azurewebsites.net", "http://localhost:4200" )
                .AllowAnyMethod()
                .AllowCredentials()
                .AllowAnyHeader()));

            services.AddAutoMapper(c =>
                c.AddProfiles(AppDomain.CurrentDomain.GetAssemblies().Where(t => t.FullName.Contains("Eriks"))));

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(x => {
                    x.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    x.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                });
            services.AddSwaggerGen(c => {
                c.SwaggerDoc(_apiVersion, new Info { Title = _appTitle, Version = _apiVersion });

                var dir = new DirectoryInfo(System.AppContext.BaseDirectory);
                foreach (var file in dir.GetFiles("*.xml")) {
                    var filePath = Path.Combine(dir.FullName, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml");
                    c.IncludeXmlComments(file.FullName);
                }
            });

            services.AddTransient<ICategoriesRepository, CategoriesRepository>();
            services.AddTransient<IProductsRepository, ProductsRepository>();

            services.AddDbContext<EriksContext>(
                options => {
                    options.UseSqlServer(
                       Configuration.GetConnectionString(nameof(EriksContext)),
                       sqlServerOptions => sqlServerOptions.CommandTimeout(300)
                    );
                },
                ServiceLifetime.Scoped);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            app.UseStaticFiles();

            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            } else {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
                app.UseHttpsRedirection();
            }

            app.UseCors("AllowAll");
            app.UseMvc();

            app.UseSwagger(c => {
                c.PreSerializeFilters.Add((swaggerDoc, httpReq) => swaggerDoc.Host = httpReq.Host.Value);
            });

            app.UseSwaggerUI(c => {
                // when change API version don't forget to change json url in the redoc html file in swagger folder
                c.SwaggerEndpoint($"/swagger/{_apiVersion}/swagger.json", $"{_appTitle} {_apiVersion}");
                c.IndexStream = () =>
                    GetType().GetTypeInfo().Assembly.GetManifestResourceStream($"{_appTitle}.swagger.redoc_page.html");
            });

            // Apply migrations to the database
            app.UpdateDatabase();
        }

    }
}
