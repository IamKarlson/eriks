﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CsvHelper.Configuration.Attributes;
using Eriks.Data.Contracts.Access;

namespace Eriks.Data.Contracts.Entities
{
    public class Category : IIdEntity<string> {
        [Key]
        [Name("CategoryID")]
        public string Id { get; set; }

        public string Name { get; set; }

        public virtual List<Product> Products { get; set; }
        public DateTime Updated { get; set; }
    }
}
