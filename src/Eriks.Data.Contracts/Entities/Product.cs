﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CsvHelper.Configuration.Attributes;
using Eriks.Data.Contracts.Access;

namespace Eriks.Data.Contracts.Entities {
    public class Product : IIdEntity<string> {
        [Key]
        [Name("ZamroID")]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal? MinOrderQuantity { get; set; }
        public string UnitOfMeasure { get; set; }
        [ForeignKey(nameof(CategoryID))]
        public virtual Category Category { get; set; }
        public string CategoryID { get; set; }
        public decimal? PurchasePrice { get; set; }
        public bool? Available { get; set; }
        public DateTime Updated { get; set; }
    }
}
