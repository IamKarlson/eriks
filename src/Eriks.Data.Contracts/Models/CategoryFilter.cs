﻿namespace Eriks.Data.Contracts.Models {
    public class CategoryFilter {
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
