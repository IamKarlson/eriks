﻿using System.Collections.Generic;

namespace Eriks.Data.Contracts.Models
{
    public class ApiResponse<T> {
        public ApiResponse() { }

        public ApiResponse(IEnumerable<T> data, int totalCount) {
            this.Data = data;
            this.TotalCount = totalCount;
        }
        public IEnumerable<T> Data { get; set; }
        public int TotalCount { get; set; }
    }
}
