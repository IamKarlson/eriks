﻿namespace Eriks.Data.Contracts.Models
{
    public class ProductFilter {
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public string CategoryId { get; set; }

        public bool? Available { get; set; }
    }
}
