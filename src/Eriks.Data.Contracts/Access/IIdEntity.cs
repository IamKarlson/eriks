﻿using System;

namespace Eriks.Data.Contracts.Access
{
    public interface IIdEntity<T> {
        T Id { get; }
        DateTime Updated { get; set; }
    }
}
