﻿using System;

namespace Eriks.Data.Contracts.Access
{
    public abstract class GuidIdEntity : IIdEntity<Guid> {
        public Guid Id { get; set; }

        public DateTime Updated { get; set; }
    }
}
