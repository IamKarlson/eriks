﻿namespace Eriks.Data.Contracts.Access
{
    public class Paging {
        public int? Take { get; set; }

        public int? Skip { get; set; }
    }
}
