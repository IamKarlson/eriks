﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Eriks.Data.Contracts.Access {
    public interface IRepository<T, TId, in TFilter> where T : IIdEntity<TId> where TFilter : class {
        Task<T> Get(TId id);
        Task<int> HowMany(TFilter filter);
        Task<List<T>> GetMany(TFilter filter, Paging paging = null, bool includeReferences = false);
        Task Update(TId id, T entity);
        Task<T> Delete(TId id);
        Task<T> Create(T entity);
        Task<IEnumerable<TId>> CreateMany(IEnumerable<T> entity);
    }
}
