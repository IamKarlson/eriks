﻿using Eriks.Data.Contracts.Entities;
using Microsoft.EntityFrameworkCore;

namespace Eriks.Data {
    public partial class EriksContext : DbContext {

        public EriksContext(DbContextOptions<EriksContext> options) : base(options) {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            SeedData(modelBuilder);
        }

        public DbSet<Product> Products { get; set; }

        public DbSet<Category> Categories { get; set; }
    }

}
