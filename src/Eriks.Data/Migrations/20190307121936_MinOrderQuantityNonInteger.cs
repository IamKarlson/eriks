﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Eriks.Data.Migrations
{
    public partial class MinOrderQuantityNonInteger : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "MinOrderQuantity",
                table: "Products",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "MinOrderQuantity",
                table: "Products",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true);
        }
    }
}
