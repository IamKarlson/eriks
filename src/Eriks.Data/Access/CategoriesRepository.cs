﻿using System.Linq;
using Eriks.Data.Contracts.Entities;
using Eriks.Data.Contracts.Models;
using Microsoft.EntityFrameworkCore;

namespace Eriks.Data.Access {
    public class CategoriesRepository : BaseRepository<Category, string, CategoryFilter>, ICategoriesRepository {
        public CategoriesRepository(EriksContext context) : base(context) {
        }

        protected override DbSet<Category> DbSet => Context.Categories;
        protected override IQueryable<Category> ApplyFiltering(IQueryable<Category> query, CategoryFilter filter) {
            if (!string.IsNullOrWhiteSpace(filter.CategoryId)) {
                query = query.Where(t =>
                    t.Id.Contains(filter.CategoryId)
                );
            }

            if (!string.IsNullOrWhiteSpace(filter.CategoryName)) {
                query = query.Where(t =>
                    t.Name.Contains(filter.CategoryName)
                    );
            }

            return query;
        }

        protected override void prepareEntityForUpsert(Category entity) {
        }
    }
}
