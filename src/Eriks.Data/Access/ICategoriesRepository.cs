﻿using Eriks.Data.Contracts.Access;
using Eriks.Data.Contracts.Entities;
using Eriks.Data.Contracts.Models;

namespace Eriks.Data.Access
{
    public interface ICategoriesRepository : IRepository<Category, string, CategoryFilter> {
    }
}