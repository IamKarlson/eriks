﻿using Eriks.Data.Contracts.Access;
using Eriks.Data.Contracts.Entities;
using Eriks.Data.Contracts.Models;

namespace Eriks.Data.Access
{
    public interface IProductsRepository : IRepository<Product, string, ProductFilter> {
    }
}