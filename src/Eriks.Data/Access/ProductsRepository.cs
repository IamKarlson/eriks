﻿using System.Linq;
using Eriks.Data.Contracts.Entities;
using Eriks.Data.Contracts.Models;
using Microsoft.EntityFrameworkCore;

namespace Eriks.Data.Access {
    public class ProductsRepository : BaseRepository<Product, string, ProductFilter>, IProductsRepository {
        public ProductsRepository(EriksContext context) : base(context) {
        }

        protected override DbSet<Product> DbSet => Context.Products;

        protected override IQueryable<Product> ApplyInclude(IQueryable<Product> query) {
            query = query.Include(t=>t.Category);
            return query;
        }
        protected override IQueryable<Product> ApplyFiltering(IQueryable<Product> query, ProductFilter filter) {
            if (!string.IsNullOrWhiteSpace(filter.ProductId)) {
                query = query.Where(t => t.Id.Contains(filter.ProductId));
            }

            if (!string.IsNullOrWhiteSpace(filter.ProductName)) {
                query = query.Where(t => t.Name.Contains(filter.ProductName));
            }

            if (!string.IsNullOrWhiteSpace(filter.CategoryId)) {
                query = query.Where(t => t.Category.Name.Contains(filter.CategoryId));
            }
            if (filter.Available.HasValue) {
                query = query.Where(t => t.Available ==filter.Available.Value);
            }

            return query;
        }

        protected override void prepareEntityForUpsert(Product entity) {
            entity.Category = null;
        }
    }

}
