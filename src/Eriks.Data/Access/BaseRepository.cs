﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFCore.BulkExtensions;
using Eriks.Data.Contracts.Access;
using Microsoft.EntityFrameworkCore;

namespace Eriks.Data.Access {
    public abstract class BaseRepository<T, TId, TFilter> : IRepository<T, TId, TFilter> where TFilter : class where T : class, IIdEntity<TId> {

        protected readonly EriksContext Context;

        public BaseRepository(EriksContext context) {
            Context = context;
        }

        protected abstract DbSet<T> DbSet { get; }

        protected virtual IQueryable<T> ApplyFiltering(IQueryable<T> query, TFilter filter) => query;

        protected virtual IQueryable<T> ApplyInclude(IQueryable<T> query) => query;
        protected virtual IQueryable<T> ApplySorting(IQueryable<T> query) => query;

        public virtual async Task<T> Get(TId id) {
            var query = DbSet.AsQueryable();
            query = ApplyInclude(query);
            query = query.Where(t => Equals(t.Id, id));
            return await query
                .SingleOrDefaultAsync();
        }

        public async Task<int> HowMany(TFilter filter) {
            var query = DbSet.AsQueryable();

            query = ApplyFiltering(query, filter);
            return await query.CountAsync();
        }

        public virtual async Task<List<T>> GetMany(TFilter filter, Paging paging = null, bool includeReferences = false) {
            var query = DbSet.AsQueryable();

            if (includeReferences) {
                query = ApplyInclude(query);
            }
            query = ApplyFiltering(query, filter);

            if (paging != null) { query = ApplyPaging(query, paging); }
            query = ApplySorting(query);

            return await query.ToListAsync();
        }


        public virtual async Task<T> Create(T entity) {
            try {
                entity.Updated = DateTime.Now;
                DbSet.Add(entity);
                await Context.SaveChangesAsync();
                return entity;

            } catch {
                RejectChanges();
                throw;
            }
        }
        public virtual async Task<IEnumerable<TId>> CreateMany(IEnumerable<T> entities) {
            try {
                List<T> list = entities.ToList();
                foreach (var entity in list) {
                    entity.Updated = DateTime.Now;
                    prepareEntityForUpsert(entity);
                }

                await Context.BulkInsertOrUpdateAsync(list, new BulkConfig() { BulkCopyTimeout = 600 });

                await Context.SaveChangesAsync();
                return list.Select(t => t.Id);

            } catch {
                RejectChanges();
                throw;
            }
        }

        protected abstract void prepareEntityForUpsert(T entity);

        public void RejectChanges() {
            var entries = Context.ChangeTracker.Entries().ToList();
            foreach (var entry in entries) {
                switch (entry.State) {
                    case EntityState.Modified:
                    case EntityState.Deleted:
                        entry.State = EntityState.Modified; //Revert changes made to deleted entity.
                        entry.State = EntityState.Unchanged;
                        break;
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                }
            }
        }

        public virtual async Task Update(TId id, T entity) {
            try {
                Context.Entry(entity).State = EntityState.Modified;
                entity.Updated = DateTime.Now;
                await Context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException) {
                if (!EntityExists(id)) {
                    throw new ArgumentOutOfRangeException(nameof(id));
                }
                RejectChanges();
                throw;
            }
        }

        public virtual async Task<T> Delete(TId id) {
            try {
                var entity = await Get(id);
                if (entity == null) {
                    throw new ArgumentOutOfRangeException(nameof(id));
                }

                entity.Updated = DateTime.Now;
                DbSet.Remove(entity);
                await Context.SaveChangesAsync();
                return entity;
            } catch {
                RejectChanges();
                throw;
            }
        }


        private bool EntityExists(TId id) {
            var query = DbSet.AsQueryable();
            return query.Any(e => Equals(e.Id, id));
        }
        private IQueryable<T> ApplyPaging(IQueryable<T> query, Paging paging) {
            int? take = paging?.Take;
            int? skip = paging?.Skip;
            if (!take.HasValue && !skip.HasValue) {
                return query;
            }

            if (skip.HasValue) {
                query = query.Skip(skip.Value);
            }

            if (take.HasValue) {
                query = query.Take(take.Value);
            }

            return query;
        }

    }
}
