﻿using Eriks.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Eriks {
    public static class AppExts {
        public static void UpdateDatabase(this IApplicationBuilder app) {
            using (var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope()) {
                using (var context = serviceScope.ServiceProvider.GetService<EriksContext>()) {
                    context.Database.Migrate();
                }
            }
        }
    }
}
