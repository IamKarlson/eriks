﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using Eriks.Data.Contracts.Access;
using Eriks.Data.Contracts.Entities;
using Eriks.Data.Contracts.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Eriks.Core {
    /// <summary>
    /// Generic controller attributed with common properties
    /// </summary>
    [Route("api/v1/[controller]")]
    [ApiController]
    public abstract class ApiControllerBase<T, TId, TFilter> : ControllerBase where T : IIdEntity<TId> where TFilter : class {
        protected readonly IRepository<T, TId, TFilter> _repository;
        protected readonly ILogger _logger;

        protected virtual bool includeReferencesInListResults => false;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="repository">Objects DAL object</param>
        /// <param name="logger">Standard logging interface</param>
        public ApiControllerBase(IRepository<T, TId, TFilter> repository, ILogger logger) {
            _repository = repository;
            _logger = logger;
        }


        /// <summary>
        /// Get all objects satisfying provided criteria
        /// </summary>
        /// <param name="filter">Objects filter</param>
        /// <param name="paging">Paging parameters</param>
        /// <returns>Objects search result</returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<ApiResponse<T>>> GetMany([FromQuery] TFilter filter = null,
            [FromQuery] Paging paging = null) {
            var count = await _repository.HowMany(filter);
            var objects = await _repository.GetMany(filter, paging, includeReferencesInListResults);

            return new ApiResponse<T>(objects, count);
        }


        /// <summary>
        /// Get single object
        /// </summary>
        /// <param name="id">Object id</param>
        /// <returns>T object</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<T>> GetT(TId id) {
            var model = await _repository.Get(id);

            if (model == null) {
                return NotFound();
            }

            return model;
        }

        /// <summary>
        /// Update model information
        /// </summary>
        /// <param name="id">Object id</param>
        /// <param name="model">Modified model data</param>
        /// <returns>Operation result</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutT(TId id, T model) {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }
            if (!Equals(id, model.Id)) {
                return BadRequest();
            }


            try {
                await _repository.Update(id, model);
            } catch (ArgumentOutOfRangeException) {
                return NotFound();
            }

            return NoContent();
        }

        /// <summary>
        /// Create new model
        /// </summary>
        /// <param name="model">New model data</param>
        /// <returns>Saved model data</returns>
        [HttpPost]
        public async Task<ActionResult<T>> PostT(T model) {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }
            var newT = await _repository.Create(model);

            return CreatedAtAction(nameof(GetT), new { id = newT.Id }, newT);
        }

        [HttpPost("UploadFile")]
        public async Task<IActionResult> UploadFile([FromForm(Name = "file")] IFormFile formFile) {

            // full path to file in temp location
            var filePath = Path.GetTempFileName();

            var ids = new List<TId>();
            if (formFile.Length > 0) {
                using (var stream = new FileStream(filePath, FileMode.Create)) {
                    await formFile.CopyToAsync(stream);
                    stream.Position = 0;
                    using (var reader = new StreamReader(stream, Encoding.UTF8)) {
                        var csv = new CsvReader(reader);
                        csv.Configuration.Delimiter = ",";
                        csv.Configuration.CultureInfo = CultureInfo.InvariantCulture;
                        csv.Configuration.HeaderValidated = null;
                        csv.Configuration.ShouldSkipRecord = record => record.All(string.IsNullOrWhiteSpace);
                        csv.Configuration.MissingFieldFound = (headers, i, arg3) => {
                            _logger.LogWarning("Some fields aren't found in CSV");
                        };
                        var records = csv.GetRecords<T>();
                        var newlyCreatedIds = await this._repository.CreateMany(records.ToList());
                        ids.AddRange(newlyCreatedIds);
                    }
                }
            }

            // process uploaded files
            // Don't rely on or trust the FileName property without validation.
            return Ok(new { ids });
        }
        [HttpGet("DownloadFile")]
        public async Task<FileStreamResult> DownloadFile([FromQuery] TFilter filter = null) {
            var objects = await _repository.GetMany(filter);
            using (MemoryStream ms = new MemoryStream())
            using (StreamWriter sw = new StreamWriter(ms, Encoding.UTF8))
            using (CsvWriter csv = new CsvWriter(sw)) {
                csv.Configuration.Delimiter = ",";
                csv.Configuration.CultureInfo = CultureInfo.InvariantCulture;
                csv.Configuration.ShouldQuote = (field, context) => true;

                csv.WriteRecords(objects);

                //ms.Position = 0;
                sw.Flush();

                return new FileStreamResult(new MemoryStream(ms.ToArray()), "text/csv") { FileDownloadName = $"{typeof(T).Name.ToLower()}.csv" };
            }
        }

        /// <summary>
        /// Delete particular model by its id
        /// </summary>
        /// <param name="id">Object id</param>
        /// <returns>Deleted model data</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<T>> DeleteT(TId id) {
            try {
                var model = await _repository.Delete(id);
                return model;
            } catch (ArgumentOutOfRangeException) {
                return NotFound();
            }
        }
    }
}
