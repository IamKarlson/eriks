/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ApiResponseCategory } from '../models/api-response-category';
import { Category } from '../models/category';
@Injectable({
  providedIn: 'root',
})
class CategoriesService extends __BaseService {
  static readonly GetManyPath = '/api/v1/Categories';
  static readonly PostTPath = '/api/v1/Categories';
  static readonly GetTPath = '/api/v1/Categories/{id}';
  static readonly PutTPath = '/api/v1/Categories/{id}';
  static readonly DeleteTPath = '/api/v1/Categories/{id}';
  static readonly UploadFilePath = '/api/v1/Categories/UploadFile';
  static readonly DownloadFilePath = '/api/v1/Categories/DownloadFile';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param params The `CategoriesService.GetManyParams` containing the following parameters:
   *
   * - `Take`:
   *
   * - `Skip`:
   *
   * - `CategoryName`:
   *
   * - `CategoryId`:
   *
   * @return Success
   */
  GetManyResponse(params: CategoriesService.GetManyParams): __Observable<__StrictHttpResponse<ApiResponseCategory>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.Take != null) __params = __params.set('Take', params.Take.toString());
    if (params.Skip != null) __params = __params.set('Skip', params.Skip.toString());
    if (params.CategoryName != null) __params = __params.set('CategoryName', params.CategoryName.toString());
    if (params.CategoryId != null) __params = __params.set('CategoryId', params.CategoryId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/Categories`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ApiResponseCategory>;
      })
    );
  }
  /**
   * @param params The `CategoriesService.GetManyParams` containing the following parameters:
   *
   * - `Take`:
   *
   * - `Skip`:
   *
   * - `CategoryName`:
   *
   * - `CategoryId`:
   *
   * @return Success
   */
  GetMany(params: CategoriesService.GetManyParams): __Observable<ApiResponseCategory> {
    return this.GetManyResponse(params).pipe(
      __map(_r => _r.body as ApiResponseCategory)
    );
  }

  /**
   * @param model New model data
   * @return Success
   */
  PostTResponse(model?: Category): __Observable<__StrictHttpResponse<Category>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = model;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/Categories`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Category>;
      })
    );
  }
  /**
   * @param model New model data
   * @return Success
   */
  PostT(model?: Category): __Observable<Category> {
    return this.PostTResponse(model).pipe(
      __map(_r => _r.body as Category)
    );
  }

  /**
   * @param id Object id
   * @return Success
   */
  GetTResponse(id: string): __Observable<__StrictHttpResponse<Category>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/Categories/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Category>;
      })
    );
  }
  /**
   * @param id Object id
   * @return Success
   */
  GetT(id: string): __Observable<Category> {
    return this.GetTResponse(id).pipe(
      __map(_r => _r.body as Category)
    );
  }

  /**
   * @param params The `CategoriesService.PutTParams` containing the following parameters:
   *
   * - `id`: Object id
   *
   * - `model`: Modified model data
   */
  PutTResponse(params: CategoriesService.PutTParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.model;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/v1/Categories/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param params The `CategoriesService.PutTParams` containing the following parameters:
   *
   * - `id`: Object id
   *
   * - `model`: Modified model data
   */
  PutT(params: CategoriesService.PutTParams): __Observable<null> {
    return this.PutTResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id Object id
   * @return Success
   */
  DeleteTResponse(id: string): __Observable<__StrictHttpResponse<Category>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v1/Categories/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Category>;
      })
    );
  }
  /**
   * @param id Object id
   * @return Success
   */
  DeleteT(id: string): __Observable<Category> {
    return this.DeleteTResponse(id).pipe(
      __map(_r => _r.body as Category)
    );
  }

  /**
   * @param params The `CategoriesService.UploadFileParams` containing the following parameters:
   *
   * - `Name`:
   *
   * - `Length`:
   *
   * - `Headers`:
   *
   * - `FileName`:
   *
   * - `ContentType`:
   *
   * - `ContentDisposition`:
   */
  UploadFileResponse(params: CategoriesService.UploadFileParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __headers.append('Content-Type', 'multipart/form-data');
    let __formData = new FormData();
    __body = __formData;
   if(params.Name !== null && typeof params.Name !== "undefined") { __formData.append('Name', params.Name as string | Blob);}
   if(params.Length !== null && typeof params.Length !== "undefined") { __formData.append('Length', JSON.stringify(params.Length));}
   if(params.Headers !== null && typeof params.Headers !== "undefined") { __formData.append('Headers', JSON.stringify(params.Headers));}
   if(params.FileName !== null && typeof params.FileName !== "undefined") { __formData.append('FileName', params.FileName as string | Blob);}
   if(params.ContentType !== null && typeof params.ContentType !== "undefined") { __formData.append('ContentType', params.ContentType as string | Blob);}
   if(params.ContentDisposition !== null && typeof params.ContentDisposition !== "undefined") { __formData.append('ContentDisposition', params.ContentDisposition as string | Blob);}
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/Categories/UploadFile`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param params The `CategoriesService.UploadFileParams` containing the following parameters:
   *
   * - `Name`:
   *
   * - `Length`:
   *
   * - `Headers`:
   *
   * - `FileName`:
   *
   * - `ContentType`:
   *
   * - `ContentDisposition`:
   */
  UploadFile(params: CategoriesService.UploadFileParams): __Observable<null> {
    return this.UploadFileResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param params The `CategoriesService.DownloadFileParams` containing the following parameters:
   *
   * - `CategoryName`:
   *
   * - `CategoryId`:
   */
  DownloadFileResponse(params: CategoriesService.DownloadFileParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.CategoryName != null) __params = __params.set('CategoryName', params.CategoryName.toString());
    if (params.CategoryId != null) __params = __params.set('CategoryId', params.CategoryId.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/Categories/DownloadFile`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param params The `CategoriesService.DownloadFileParams` containing the following parameters:
   *
   * - `CategoryName`:
   *
   * - `CategoryId`:
   */
  DownloadFile(params: CategoriesService.DownloadFileParams): __Observable<null> {
    return this.DownloadFileResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module CategoriesService {

  /**
   * Parameters for GetMany
   */
  export interface GetManyParams {
    Take?: number;
    Skip?: number;
    CategoryName?: string;
    CategoryId?: string;
  }

  /**
   * Parameters for PutT
   */
  export interface PutTParams {

    /**
     * Object id
     */
    id: string;

    /**
     * Modified model data
     */
    model?: Category;
  }

  /**
   * Parameters for UploadFile
   */
  export interface UploadFileParams {
    Name?: string;
    Length?: number;
    Headers?: {};
    FileName?: string;
    ContentType?: string;
    ContentDisposition?: string;
  }

  /**
   * Parameters for DownloadFile
   */
  export interface DownloadFileParams {
    CategoryName?: string;
    CategoryId?: string;
  }
}

export { CategoriesService }
