/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { ApiResponseProduct } from '../models/api-response-product';
import { Product } from '../models/product';
@Injectable({
  providedIn: 'root',
})
class ProductsService extends __BaseService {
  static readonly GetManyPath = '/api/v1/Products';
  static readonly PostTPath = '/api/v1/Products';
  static readonly GetTPath = '/api/v1/Products/{id}';
  static readonly PutTPath = '/api/v1/Products/{id}';
  static readonly DeleteTPath = '/api/v1/Products/{id}';
  static readonly UploadFilePath = '/api/v1/Products/UploadFile';
  static readonly DownloadFilePath = '/api/v1/Products/DownloadFile';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param params The `ProductsService.GetManyParams` containing the following parameters:
   *
   * - `Take`:
   *
   * - `Skip`:
   *
   * - `ProductName`:
   *
   * - `ProductId`:
   *
   * - `CategoryId`:
   *
   * - `Available`:
   *
   * @return Success
   */
  GetManyResponse(params: ProductsService.GetManyParams): __Observable<__StrictHttpResponse<ApiResponseProduct>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.Take != null) __params = __params.set('Take', params.Take.toString());
    if (params.Skip != null) __params = __params.set('Skip', params.Skip.toString());
    if (params.ProductName != null) __params = __params.set('ProductName', params.ProductName.toString());
    if (params.ProductId != null) __params = __params.set('ProductId', params.ProductId.toString());
    if (params.CategoryId != null) __params = __params.set('CategoryId', params.CategoryId.toString());
    if (params.Available != null) __params = __params.set('Available', params.Available.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/Products`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<ApiResponseProduct>;
      })
    );
  }
  /**
   * @param params The `ProductsService.GetManyParams` containing the following parameters:
   *
   * - `Take`:
   *
   * - `Skip`:
   *
   * - `ProductName`:
   *
   * - `ProductId`:
   *
   * - `CategoryId`:
   *
   * - `Available`:
   *
   * @return Success
   */
  GetMany(params: ProductsService.GetManyParams): __Observable<ApiResponseProduct> {
    return this.GetManyResponse(params).pipe(
      __map(_r => _r.body as ApiResponseProduct)
    );
  }

  /**
   * @param model New model data
   * @return Success
   */
  PostTResponse(model?: Product): __Observable<__StrictHttpResponse<Product>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = model;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/Products`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Product>;
      })
    );
  }
  /**
   * @param model New model data
   * @return Success
   */
  PostT(model?: Product): __Observable<Product> {
    return this.PostTResponse(model).pipe(
      __map(_r => _r.body as Product)
    );
  }

  /**
   * @param id Object id
   * @return Success
   */
  GetTResponse(id: string): __Observable<__StrictHttpResponse<Product>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/Products/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Product>;
      })
    );
  }
  /**
   * @param id Object id
   * @return Success
   */
  GetT(id: string): __Observable<Product> {
    return this.GetTResponse(id).pipe(
      __map(_r => _r.body as Product)
    );
  }

  /**
   * @param params The `ProductsService.PutTParams` containing the following parameters:
   *
   * - `id`: Object id
   *
   * - `model`: Modified model data
   */
  PutTResponse(params: ProductsService.PutTParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    __body = params.model;
    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/v1/Products/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param params The `ProductsService.PutTParams` containing the following parameters:
   *
   * - `id`: Object id
   *
   * - `model`: Modified model data
   */
  PutT(params: ProductsService.PutTParams): __Observable<null> {
    return this.PutTResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id Object id
   * @return Success
   */
  DeleteTResponse(id: string): __Observable<__StrictHttpResponse<Product>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'DELETE',
      this.rootUrl + `/api/v1/Products/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Product>;
      })
    );
  }
  /**
   * @param id Object id
   * @return Success
   */
  DeleteT(id: string): __Observable<Product> {
    return this.DeleteTResponse(id).pipe(
      __map(_r => _r.body as Product)
    );
  }

  /**
   * @param params The `ProductsService.UploadFileParams` containing the following parameters:
   *
   * - `Name`:
   *
   * - `Length`:
   *
   * - `Headers`:
   *
   * - `FileName`:
   *
   * - `ContentType`:
   *
   * - `ContentDisposition`:
   */
  UploadFileResponse(params: ProductsService.UploadFileParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __headers.append('Content-Type', 'multipart/form-data');
    let __formData = new FormData();
    __body = __formData;
   if(params.Name !== null && typeof params.Name !== "undefined") { __formData.append('Name', params.Name as string | Blob);}
   if(params.Length !== null && typeof params.Length !== "undefined") { __formData.append('Length', JSON.stringify(params.Length));}
   if(params.Headers !== null && typeof params.Headers !== "undefined") { __formData.append('Headers', JSON.stringify(params.Headers));}
   if(params.FileName !== null && typeof params.FileName !== "undefined") { __formData.append('FileName', params.FileName as string | Blob);}
   if(params.ContentType !== null && typeof params.ContentType !== "undefined") { __formData.append('ContentType', params.ContentType as string | Blob);}
   if(params.ContentDisposition !== null && typeof params.ContentDisposition !== "undefined") { __formData.append('ContentDisposition', params.ContentDisposition as string | Blob);}
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/v1/Products/UploadFile`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param params The `ProductsService.UploadFileParams` containing the following parameters:
   *
   * - `Name`:
   *
   * - `Length`:
   *
   * - `Headers`:
   *
   * - `FileName`:
   *
   * - `ContentType`:
   *
   * - `ContentDisposition`:
   */
  UploadFile(params: ProductsService.UploadFileParams): __Observable<null> {
    return this.UploadFileResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param params The `ProductsService.DownloadFileParams` containing the following parameters:
   *
   * - `ProductName`:
   *
   * - `ProductId`:
   *
   * - `CategoryId`:
   *
   * - `Available`:
   */
  DownloadFileResponse(params: ProductsService.DownloadFileParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.ProductName != null) __params = __params.set('ProductName', params.ProductName.toString());
    if (params.ProductId != null) __params = __params.set('ProductId', params.ProductId.toString());
    if (params.CategoryId != null) __params = __params.set('CategoryId', params.CategoryId.toString());
    if (params.Available != null) __params = __params.set('Available', params.Available.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/v1/Products/DownloadFile`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param params The `ProductsService.DownloadFileParams` containing the following parameters:
   *
   * - `ProductName`:
   *
   * - `ProductId`:
   *
   * - `CategoryId`:
   *
   * - `Available`:
   */
  DownloadFile(params: ProductsService.DownloadFileParams): __Observable<null> {
    return this.DownloadFileResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module ProductsService {

  /**
   * Parameters for GetMany
   */
  export interface GetManyParams {
    Take?: number;
    Skip?: number;
    ProductName?: string;
    ProductId?: string;
    CategoryId?: string;
    Available?: boolean;
  }

  /**
   * Parameters for PutT
   */
  export interface PutTParams {

    /**
     * Object id
     */
    id: string;

    /**
     * Modified model data
     */
    model?: Product;
  }

  /**
   * Parameters for UploadFile
   */
  export interface UploadFileParams {
    Name?: string;
    Length?: number;
    Headers?: {};
    FileName?: string;
    ContentType?: string;
    ContentDisposition?: string;
  }

  /**
   * Parameters for DownloadFile
   */
  export interface DownloadFileParams {
    ProductName?: string;
    ProductId?: string;
    CategoryId?: string;
    Available?: boolean;
  }
}

export { ProductsService }
