/* tslint:disable */
import { Category } from './category';
export interface ApiResponseCategory {
  data?: Array<Category>;
  totalCount?: number;
}
