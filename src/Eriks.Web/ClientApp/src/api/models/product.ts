/* tslint:disable */
import { Category } from './category';
export interface Product {
  id?: string;
  name?: string;
  description?: string;
  minOrderQuantity?: number;
  unitOfMeasure?: string;
  category?: Category;
  categoryID?: string;
  purchasePrice?: number;
  available?: boolean;
  updated?: string;
}
