/* tslint:disable */
import { Product } from './product';
export interface ApiResponseProduct {
  data?: Array<Product>;
  totalCount?: number;
}
