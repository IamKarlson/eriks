/* tslint:disable */
import { Product } from './product';
export interface Category {
  id?: string;
  name?: string;
  products?: Array<Product>;
  updated?: string;
}
