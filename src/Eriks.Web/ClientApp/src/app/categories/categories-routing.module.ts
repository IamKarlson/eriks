import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoriesUploadComponent } from './categories-upload/categories-upload.component';
import { CategoriesComponent } from './categories-list/categories.component';
import { CategoryComponent } from './category-details/category.component';

const routes: Routes = [
      { path: 'categories', component: CategoriesComponent },
      { path: 'category/:id', component: CategoryComponent },
      { path: 'categoriesUpload', component: CategoriesUploadComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoriesRoutingModule { }
