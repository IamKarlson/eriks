import { Component, OnInit, ViewChild } from '@angular/core';
import { CategoriesService } from 'src/api/services';
import { FileUploader, FileSelectDirective, FileItem, ParsedResponseHeaders } from 'ng2-file-upload/ng2-file-upload';
import { ApiConfiguration } from 'src/api/api-configuration';
import { NgStyle, NgClass } from '@angular/common';
import { Router } from '@angular/router';



@Component({
  selector: 'app-categories-upload',
  templateUrl: './categories-upload.component.html',
  styleUrls: ['./categories-upload.component.css']
})
export class CategoriesUploadComponent implements OnInit {

  @ViewChild('categoriesFile') categoriesFile: any;

  public uploader: FileUploader;
  public error: any;
  public status: boolean;

  constructor(private categoriesService: CategoriesService,
    protected config: ApiConfiguration,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.uploader = new FileUploader({ url: this.config.rootUrl + `/api/v1/Categories/UploadFile` });
    this.uploader.onErrorItem = (item, response, status, headers) => this.onErrorItem(item, response, status, headers);
    this.uploader.onSuccessItem = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers);
  }

  onSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    // this gets triggered only once when first file is uploaded
    this.router.navigate(['/categories']);
  }

  onErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    this.error = JSON.parse(response); // error server response
  }
  public upload() {
    this.status = true;
    this.uploader.uploadAll();
  }
}
