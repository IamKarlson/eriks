import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriesUploadComponent } from './categories-upload.component';

describe('CategoriesUploadComponent', () => {
  let component: CategoriesUploadComponent;
  let fixture: ComponentFixture<CategoriesUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoriesUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriesUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
