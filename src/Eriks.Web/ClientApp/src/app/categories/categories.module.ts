import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FileUploadModule } from 'ng2-file-upload';

import { CategoriesRoutingModule } from './categories-routing.module';
import { CategoriesUploadComponent } from './categories-upload/categories-upload.component';
import { ApiModule } from 'src/api/api.module';
import { FormsModule } from '@angular/forms';
import { CategoriesComponent } from './categories-list/categories.component';
import { CategoryComponent } from './category-details/category.component';

@NgModule({
  imports: [
    CommonModule,
    ApiModule,
    FormsModule,
    FileUploadModule,
    NgbModule,
    CategoriesRoutingModule
  ],
  declarations: [CategoriesComponent,CategoryComponent,CategoriesUploadComponent]
})
export class CategoriesModule { }
