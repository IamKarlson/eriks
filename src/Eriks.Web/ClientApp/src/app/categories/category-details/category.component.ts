import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from 'src/api/models';
import { ActivatedRoute } from '@angular/router';
import { CategoriesService } from 'src/api/services';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  category: Observable<Category>;

  constructor(
    private route: ActivatedRoute,
    private categorysService: CategoriesService) { }

  ngOnInit() {
    let categoryId = this.route.snapshot.paramMap.get('id');
    this.category = this.categorysService.GetT(categoryId);
  }
}