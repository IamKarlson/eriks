import { Component, OnInit } from '@angular/core';
import { CategoriesService } from 'src/api/services';
import { Category } from 'src/api/models';
import { ApiConfiguration } from 'src/api/api-configuration';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  categories: Category[];
  page: number;
  totalCategories: number;
  loaderText: string;
  categoryId = '';
  categoryName = '';

  constructor(private categoriesService: CategoriesService,
    private apiConfiguration: ApiConfiguration) { }

  ngOnInit() {
    this.page = 1;
    this.Search();
    this.loaderText = 'Loading Categories Data...';
  }

  public pageChanged(selectedPage: number) {
    this.page = selectedPage;
    this.Search();
  }
  public Search() {
    this.loaderText = 'Loading Categories Data...';
    const skip = (this.page - 1) * 10;
    this.categories = [];
    const filter = {
      Skip: skip,
      Take: 10,
      CategoryId: this.categoryId,
      CategoryName: this.categoryName,
    };
    this.categoriesService.GetMany(filter).subscribe(result => {
      if (!result.data || result.data.length === 0) {
        this.loaderText = 'Categories satisfying passed parameters are not found';
        return;
      }
      this.categories = result.data;
      this.totalCategories = result.totalCount;
    }, error => console.error(error));
  }

  public Download() {

    const filter = {
      CategoryId: this.categoryId,
      CategoryName: this.categoryName,
    };
    const url = `${this.apiConfiguration.rootUrl}/api/v1/Categories/DownloadFile`
      + `?CategoryName=${this.categoryName}`
      + `&CategoryId=c${this.categoryId}`;
    window.open(url);
  }
}

