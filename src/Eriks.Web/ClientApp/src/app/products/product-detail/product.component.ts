import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/api/services';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/api/models';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  product: Observable<Product>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private productsService: ProductsService) { }

  ngOnInit() {
    let productId = this.route.snapshot.paramMap.get('id');
    this.product = this.productsService.GetT(productId);
  }

  public delete(product: Product) {
    this.productsService.DeleteT(product.id).subscribe(res => {
      console.log(`deleted ${res.id}`);

      this.router.navigate(['/products']);
    }, err => {
      console.log(`error on delete: ${err}`);
    })
  }
}
