import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductsService } from 'src/api/services';
import { FileUploader, FileItem, ParsedResponseHeaders } from 'ng2-file-upload/ng2-file-upload';
import { ApiConfiguration } from 'src/api/api-configuration';
import { Router } from '@angular/router';



@Component({
  selector: 'app-products-upload',
  templateUrl: './products-upload.component.html',
  styleUrls: ['./products-upload.component.css']
})
export class ProductsUploadComponent implements OnInit {

  @ViewChild('productsFile') productsFile: any;

  public uploader: FileUploader;
  error: any;

  constructor(private productsService: ProductsService,
    protected config: ApiConfiguration,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.uploader = new FileUploader({ url: this.config.rootUrl + `/api/v1/Products/UploadFile` });
    this.uploader.onErrorItem = (item, response, status, headers) => this.onErrorItem(item, response, status, headers);
    this.uploader.onSuccessItem = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers);
  }

  onSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    // this gets triggered only once when first file is uploaded
    this.router.navigate(['/products']);
  }

  onErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    this.error = JSON.parse(response); // error server response
  }
}
