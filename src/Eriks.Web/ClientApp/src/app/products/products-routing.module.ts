import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsUploadComponent } from './products-upload/products-upload.component';
import { ProductsComponent } from './products-list/products.component';
import { ProductComponent } from './product-detail/product.component';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { ProductCreateComponent } from './product-create/product-create.component';

const routes: Routes = [
  { path: 'products', component: ProductsComponent },
  { path: 'products/create', component: ProductCreateComponent },
  { path: 'products/:category', component: ProductsComponent },
  { path: 'product/:id', component: ProductComponent },
  { path: 'product/edit/:id', component: ProductEditComponent },
  { path: 'productsUpload', component: ProductsUploadComponent }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
