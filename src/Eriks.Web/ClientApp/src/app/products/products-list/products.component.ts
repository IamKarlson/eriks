import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { ApiConfiguration } from 'src/api/api-configuration';
import { Product } from 'src/api/models';
import { CategoriesService, ProductsService } from 'src/api/services';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products: Product[];
  page: number;
  totalProducts: number;
  loaderText: string;
  productId = '';
  productName = '';
  productCategory = '';
  productAvailability: string;

  constructor(
    private route: ActivatedRoute,
    private productsService: ProductsService,
    private categoriesService: CategoriesService,
    private apiConfiguration: ApiConfiguration) { }

  ngOnInit() {
    const category = this.route.snapshot.paramMap.get('category');
    if (category) {
      this.productCategory = category;
    }
    this.page = 1;
    this.Search();
    this.loaderText = 'Loading Products Data...';
  }

  public pageChanged(selectedPage: number) {
    this.page = selectedPage;
    this.Search();
  }

  typeaheadCategories = (text$: Observable<string>) => text$.pipe(
    debounceTime(300),
    distinctUntilChanged(),
    switchMap(term => {
      if (term.length < 2) {
        return [];
      } else {
        const filter = { CategoryName: term, Take: 10, Skip: 0 };
        return this.categoriesService.GetMany(filter)
          .pipe(map(categories => {
            const names: string[] = [];
            for (let i = 0; i < categories.data.length; i++) {
              names.push(categories.data[i].name);
            }
            return names;
          }))
          .pipe(
            catchError(() => {
              return of([]);
            }));
      }
    }),
  )

  public Search() {
    this.loaderText = 'Loading Products Data...';
    const skip = (this.page - 1) * 10;
    this.products = [];
    let available = null;
    switch (this.productAvailability) {
      case 'yes':
        available = true;
        break;
      case 'no':
        available = false;
        break;
      default:
        available = null;
        break;
    }
    const filter = {
      Skip: skip,
      Take: 10,
      ProductId: this.productId,
      ProductName: this.productName,
      CategoryId: this.productCategory,
      Available: available
    };
    this.productsService.GetMany(filter).subscribe(result => {
      if (!result.data || result.data.length === 0) {
        this.loaderText = 'Products satisfying passed parameters are not found';
        return;
      }
      this.products = result.data;
      this.totalProducts = result.totalCount;
    }, error => console.error(error));
  }
  public Download() {

    const url = `${this.apiConfiguration.rootUrl}/api/v1/Products/DownloadFile`
      + `?ProductName=${this.productName}`
      + `&ProductId=${this.productId}`
      + `&CategoryId=${this.productCategory}`;
    window.open(url);
  }

  public delete(product: Product) {
    this.productsService.DeleteT(product.id).subscribe(res => {
      console.log(`deleted ${res.id}`);
      this.Search();
    }, err => {
      console.log(`error on delete: ${err}`);
    });
  }

}

