import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { ApiModule } from 'src/api/api.module';
import { FormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProductsUploadComponent } from './products-upload/products-upload.component';
import { ProductComponent } from './product-detail/product.component';
import { ProductsComponent } from './products-list/products.component';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { ProductCreateComponent } from './product-create/product-create.component';

@NgModule({
  imports: [
    CommonModule,
    ApiModule,
    FormsModule,
    FileUploadModule,
    NgbModule,
    ProductsRoutingModule
  ],
  declarations: [ProductComponent,ProductsComponent,ProductsUploadComponent, ProductEditComponent, ProductCreateComponent]
})
export class ProductsModule { }
