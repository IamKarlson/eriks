import { Component, OnInit } from '@angular/core';
import { Category, ApiResponseCategory, Product } from 'src/api/models';
import { Observable } from 'rxjs';
import { CategoriesService, ProductsService } from 'src/api/services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css']
})
export class ProductCreateComponent implements OnInit {
  public categories: Observable<ApiResponseCategory>
  public product: Product = {} as Product;

  constructor(private categoriesService: CategoriesService,
    private productsService: ProductsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.categories = this.categoriesService.GetMany({});
  }

  create() {
    this.productsService.PostT(this.product).subscribe(res => {
      this.router.navigate(['/product', res.id]);
    }, err => {
      console.log(err);
    })
  }

}
