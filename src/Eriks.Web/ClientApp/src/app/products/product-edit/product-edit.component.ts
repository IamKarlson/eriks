import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Product } from 'src/api/models';
import { ProductsService } from 'src/api/services';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {
  product: Observable<Product>;
  productData: Product;
  productId: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private productsService: ProductsService) { }

  ngOnInit() {
    this.productId = this.route.snapshot.paramMap.get('id');
    this.product = this.productsService.GetT(this.productId);
    this.product.subscribe(res => {
      this.productData = res;
    }, err => console.log(err))
  }
  onSubmit(productForm: NgForm) {
    const newProduct = Object.assign({}, this.productData, productForm.form.value as Product);

    this.productsService.PutT({
      id:
        this.productId, model: newProduct
    }).subscribe(res => {
      this.router.navigate(['/product', this.productId]);
    })

    console.log(newProduct);
  }

}

